﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using _1_Injection.Data;
using Dapper;

namespace _1_Injection
{
    public partial class Product : System.Web.UI.Page
    {
        private SampleDbEntities db;
        protected void Page_Load(object sender, EventArgs e)
        {
            var productSubCategoryId = Convert.ToInt32(Request.QueryString["ProductSubCategoryId"]);
            /*db = new SampleDbEntities();

            var productSubCategoryId = Convert.ToInt32(Request.QueryString["ProductSubCategoryId"]);

            var results = from p in db.Products
                          where p.ProductSubcategoryID == productSubCategoryId
                          select p;

            ProductGridView.DataSource = results.ToList();
            ProductGridView.DataBind();*/

            //menggunakan dapper
            var connString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (var conn = new SqlConnection(connString))
            {
                var sqlString = @"select * from Product where ProductSubcategoryID=@ProductSubcategoryID";
                var param = new { ProductSubcategoryID = productSubCategoryId };
                List<ProductView> results = conn.Query<ProductView>(sqlString, param).ToList();

                ProductGridView.DataSource = results;
                ProductGridView.DataBind();
            }


                /*var connString = WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                var sqlString = "GetProducts";
                using (var conn = new SqlConnection(connString))
                {
                    using (var command = new SqlCommand(sqlString, conn))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        //command.Parameters.Add("@ProductSubCategoryID", SqlDbType.VarChar).Value = productSubCategoryId;
                        command.Parameters.AddWithValue("@ProductSubCategoryID", productSubCategoryId);
                        command.Connection.Open();
                        ProductGridView.DataSource = command.ExecuteReader();
                        ProductGridView.DataBind();
                    }
                }*/
                ProductCount.Text = ProductGridView.Rows.Count.ToString("n0");
        }
    }
}